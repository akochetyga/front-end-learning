'use strict';

const express = require('express');
const app = express();
const cacheManifest = require('connect-cache-manifest');

app.use(express.static(__dirname + '/public'));
app.use(cacheManifest({
  manifestPath: '/application.manifest'
}));

app.listen(8080, function () {
  console.log('front-end-learning local app listening on port 8080!');
})
