var calculator = {
  sum: function(x, y) {
    return x + y;
  },
  mult: function(x, y) {
    return x * y;
  }
};

module.exports = calculator;
