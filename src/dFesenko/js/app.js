/**
 * Created by фесенко on 05.02.2016.
 */

function sum(x, y) {

    var calcsum = x + y;
    var result = 0;
    var i = 0;
    while (i < 5){
       result += calcsum;
       i++;
    }
    return result;
}
console.log(sum());

function mult(x, y) {
    return x * y;
}
console.log(mult());

function div(x, y) {
    if (y == 0) {
    alert('Error: division by zero!');
    return  'error';
    } else {
        return x / y;
    }
}
console.log(div());

function calculate(x, y) {
    if (div(x, y) == 'error') {
        alert('Calculation error');
    } else {
        return sum(x, y) + mult(x, y) + div(x, y);
    }
}
console.log(calculate(2, 1));

var myPage = {
    title: "Bruce Lee's biography",
    setTitle: function(newTitle) {
    this.title = newTitle;
    },
    getTitle: function() {
        return this.title;
    },

    h1: "Bruce Lee",
    seth1: function(newh1) {
        this.h1 = newh1;
    },
    geth1: function() {
        return this.h1;
    },

    h2: "Информация",
    seth2: function(newh2) {
        this.h2 = newh2;
    },
    geth2: function() {
        return this.h2;
    },
//    как поступать с лишками? и повторяющимися заголовками?
    img: 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQVcCUYBgTvMFREw5JhVm5jyII3yI-yvAzXgt3tV26o6bAfYj6E',
    setImg: function(newImg) {
        this.img = newImg;
    },
    getImg: function() {
        return this.img;
    }
}
console.log(myPage.title);
console.log(myPage.setTitle('yo'));

document.getElementById("object").innerHTML = myPage.title;