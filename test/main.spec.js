var chai = require('chai');
var expect = chai.expect;
var calculator = require('./../app/calculator.js');

describe('Calculator', function() {
  it('should be able to sum 2 numbers', function() {
    expect(calculator.sum(1, 2)).to.be.equal(3);
    expect(calculator.sum(5, 17)).to.be.equal(22);
  });

  it('should be able to multiply 2 numbers', function() {
    expect(calculator.mult(1, 2)).to.be.equal(2);
    expect(calculator.mult(5, 5)).to.be.equal(25);
  });
});
